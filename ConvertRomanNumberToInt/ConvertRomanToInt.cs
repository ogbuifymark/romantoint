﻿using System;
using System.Collections.Generic;

namespace ConvertRomanNumberToInt
{
    public class ConvertRomanToInt
    {
        Dictionary<char, int> romanChar = null;
        public ConvertRomanToInt()
        {
            romanChar = new Dictionary<char, int>();
            romanChar.Add('I', 1);
            romanChar.Add('V', 5);
            romanChar.Add('X', 10);
            romanChar.Add('L', 50);
            romanChar.Add('C', 100);
            romanChar.Add('D', 500);
            romanChar.Add('M', 1000);
        }
        public int ConvertToInt(string romanVal)
        {
            romanVal = romanVal.ToUpper();
            if (romanVal.Length == 0)
            {
                return 0;
            }
            int total = 0;
            int lastVal = 0;
            for (int i = romanVal.Length- 1; i>= 0; i--)
            {
                
                if (ValidateChar(romanVal[i]))
                {
                    int currentVal = romanChar[romanVal[i]];
                    if (currentVal < lastVal)
                    {
                        total = total - currentVal;
                    }
                    else
                    {
                        total = total + currentVal;
                        lastVal = currentVal;
                    }
                }
                else
                {
                    return -1;
                }
            }
            return total;




        }
        public bool ValidateChar(char valEntered)
        {
            string RomanChars = "IVXLCDM";
            
            if (RomanChars.Contains(valEntered, StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }
            else
            {
                return false;
            }


        }
    }
}
