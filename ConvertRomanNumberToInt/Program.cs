﻿using System;
using System.Collections.Generic;

namespace ConvertRomanNumberToInt
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var convertRomanToInt = new ConvertRomanToInt();
            int convertedVal = convertRomanToInt.ConvertToInt("IX");
            if (convertedVal > 0)
            {
                Console.WriteLine(convertedVal);
            }
            else
            {
                Console.WriteLine("Value passed contains character which is not within the Roman numerical character");
            }
        }

        
    }
}
