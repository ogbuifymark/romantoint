using ConvertRomanNumberToInt;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ConvertRomanNumberToIntTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void ConvertToInt_ReturnOne()
        {
            var convertRomanToInt = new ConvertRomanToInt();
            int result = convertRomanToInt.ConvertToInt("I");

            Assert.AreEqual(result, 1);
        }
        [TestMethod]
        public void ConvertToInt_WithLowerCase_ReturnOne()
        {
            var convertRomanToInt = new ConvertRomanToInt();
            int result = convertRomanToInt.ConvertToInt("i");

            Assert.AreEqual(result, 1);
        }
        [TestMethod]
        public void ConvertToInt_ReturnNine()
        {
            var convertRomanToInt = new ConvertRomanToInt();
            int result = convertRomanToInt.ConvertToInt("IX");

            Assert.AreEqual(result, 9);
        }
        [TestMethod]
        public void ConvertToInt_ReturnEleven()
        {
            var convertRomanToInt = new ConvertRomanToInt();
            int result = convertRomanToInt.ConvertToInt("XI");

            Assert.AreEqual(result, 11);
        }
        [TestMethod]
        public void ConvertToInt_Return2015()
        {
            var convertRomanToInt = new ConvertRomanToInt();
            int result = convertRomanToInt.ConvertToInt("MMXV");

            Assert.AreEqual(result, 2015);
        }
        [TestMethod]
        public void ConvertToInt_Return1999()
        {
            var convertRomanToInt = new ConvertRomanToInt();
            int result = convertRomanToInt.ConvertToInt("MCMXCIX");

            Assert.AreEqual(result, 1999);
        }
        [TestMethod]
        public void ConvertToInt_ReturnNegative()
        {
            //this test will return negative when you pass a String which contains a Character outside the Roman Numeral 
            var convertRomanToInt = new ConvertRomanToInt();
            int result = convertRomanToInt.ConvertToInt("MCMXKCIX");

            Assert.AreEqual(result, -1);
        }

        [TestMethod]
        public void ValidateChar_ReturnTrue()
        {
            var convertRomanToInt = new ConvertRomanToInt();
            bool result = convertRomanToInt.ValidateChar('m');

            Assert.IsTrue(result);
        }
        [TestMethod]
        public void ValidateChar_ReturnFalse()
        {
            var convertRomanToInt = new ConvertRomanToInt();
            bool result = convertRomanToInt.ValidateChar('t');

            Assert.IsFalse(result);
        }
    }
}
